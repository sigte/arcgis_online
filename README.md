# ![t!](/static/justified.png) 0. Introducció a l'ArcGIS Online. 

Per tal de poder fer ús de la plataforma de publicació de mapes web, ArcGIS Online, es precís que els usuaris es registrin i es donin d'alta per a poder accedir a la plataforma. En el cas d'usuaris UdG, se'ls crearà un usuari basat en el seu usuari UdG (uxxxxxxx). Una vegada donat d'alta l'usuari, i havent generat la corresponent contrassenya, caldrà dirigir-se a la pàgina principal d'ArcGIS Online, i identificar-se: [ArcGIS online](https://www.arcgis.com/index.html#). 

![login_agol.gif!](/static/login_agol.gif "Panell d'identificació d'usuari") 

## ![t!](/static/justified.png) 1. La visualització de dades amb simbologia única segons la seva localització. 

A grans trets, AGOL (ArcGIS Online) compta amb dos apartats fonamentals que, per a l'objectiu del curs, són els que més interessen i amb els quals es treballarà de manera constant i continuada: 

  * L'espai de **Mapa**: Aquest es l'espai des d'on es configuren totes les característiques estètiques i visuals dels mapes i capes d'informació. És també en aquest apartat, des d'on es criden i es configuren les operacions d'anàlisi amb les capes seleccionades.  

  * L'espai de **Contingut**: Aquest és l'espai o repositori on s'importen i es desen totes les capes originals, les capes allotjades o capes que es deriven dels anàlisis. I és també en aquest espai on s'emmagatzemen també els arxius de mapa, que s'hauran generat des de l'espai de **Mapa**. 

### ![t!](/static/arrow-right.png) 1.1 L'apartat de «Contingut». 

Per accedir a aquest espai de treball, només cal fer un clic sobre el botó **Contingut**. En la següent imatge, es mostra l'aspecte general de l'espai de treball **«Contingut»**.  

![contingut.png!](/static/contingut.png "Aspecte de l'apartat **Contingut**") 

L'espai es divideix en diferents àrees o funcions. En primer lloc, tenim l'importador de capes que apareix sota el nom **Element nou** (ressaltat en color groc). Per altra banda, ressaltat en color verd, hi ha l'espai de gestió de carpetes. Per defecte, tots els elements que es treballen amb AGOL, es desen a l'arrel del nostre usuari. Tanmateix, AGOL permet la creació de carpetes per tal d'organitzar tots els nostres fitxers en carpetes de projecte. Per a crear una nova carpeta, tant sols cal realitzar un clic sobre la icona **Crea una carpeta**. 

En blau, es mostra l'espai de gestió de les capes, des d'on es poden editar les metadades, definir els criteris de compartició, etc... 

#### ![t!](/static/learning.png) TASCA GUIADA #1a: Importar el teu primer conjunt de dades. 

* La primera tasca, consisteix en importar una nova capa al teu espai de treball («Contingut»). La capa a importar en aquest primer cas, és un fitxer en format CSV que conté els allotjaments d'airbnb de la ciutat de Girona: [airbnb_girona.csv](/static/airbnb_girona.zip). Una vegada descarregat el fitxer al teu ordinador, cal que creïs una nova carpeta amb el nom **GIRONA**:  

![crea_carpeta.gif!](/static/crea_carpeta.gif "Creació d'una nova carpeta") 

* A continuació, fes un clic sobre el botó **Element nou** per tal d'accedir al mòdul d'importació. D'entre totes les opcions disponibles, selecciona **El vostre dispositiu**. Accedeix a la carpeta on has descarregat i descomprimit el fitxer de la pràctica, selecciona'l, i avança en el procés d'importació tal i com es mostra a continuació: 

![importa_csv!](/static/importa_csv.gif "Primeres passes de la importació") 

* En arribar al punt en el qual cal seleccionar quines columnes del fitxer CSV es volen importar, deixa marcades o seleccionades les vuit columnes que es mostren a continuació. Aprofita per editar els noms de les columnes i en acabar, fes un clic sobre el botó **Següent**: 

![columnes.png!](/static/columnes.png "Seleccionar i reanomenar columnes") 

* A la nova finestra, cal escollir el sistema de referència espacial per a la geocodificació de la capa de punts (allotjaments) que estàs important. Entre les diferents opcions, selecciona la que duu per nom **Latitud i Longitud**, i selecciona els camps que contenen les coordenades relatives als valors de latitud i longitud de cada allotjament. A continuació, fes clic sobre el botó Següent. 

* A la darrera finestra, cal que assignis un nom a la nova capa (pots deixar el nom original, també), escollir la carpeta que has creat anteriorment (GIRONA), i assignar un conjunt d'etiquetes que descriguin la capa (opcional), així com un resum (també opcional). Per acabar, fes un clic al botó **Desa**. 

* Finalment, ja pots accedir novament a l'apartat **«Contingut»** per tal de veure què hi ha: 

![resum_importa.png!](/static/resum_importa.png "Detalls de la capa") 

* A continuació, s'obre una finestra resum de la capa que just acabes d'importar. A més d'informació relativa a la capa, a la botonera de la dreta trobaràs un conjunt d'accions que t'han de permetre obrir la capa en un nou mapa (de forma automàtica, la capa seleccionada s'obrirà en un nou mapa bé al *Map Viewer Classic* bé al nou *Map Viewer*), obrir la capa al visor d'escenes (la capa en qüestió, es visualitzarà a la plataforma de visualització 3D d'AGOL), publicar la capa (es pot publicar la capa com un servei), o exportar les dades a diversos formats, entre d'altres.. 

![mostra_capes.gif!](/static/mostra_capes.gif "Capes presents a l'apartat Contingut") 

#### ![t!](/static/pencil.png) TASCA #1b: El teu torn. Importar dos nous conjunts de dades al teu espai de treball. 

A continuació, hauràs d'importar un nou conjunt de dades que conté l'oferta d'allotjament de Barcelona, i la delimitació de barris de la ciutat. Les noves capes, caldrà que les desis en una nova carpeta que durà per nom **BARCELONA**. En aquest cas, no es tracta d'un fitxer en format CSV que cal geocodificar, sinó que es tracta de dos fitxers en format .GEOJSON. El conjunt de dades a importar, és aquest: [dades_barcelona](/static/dades_bcn.zip). Tingues present a seleccionar els camps o columnes que vulguis conservar, i a editar-ne el nom si s'escau. 

### ![t!](/static/arrow-right.png) 1.2 L'apartat de «Mapa». 

En aquest apartat, és on es duen a terme totes les accions orientades al disseny i creació d'un mapa: crida de capes, simbolització, gestió de finestres emergents, etiquetes, etc. I a més, és des d'aquest apartat de **«Mapa»**, que es poden dur a terme tot un seguit d'operacions d'anàlisis amb les capes amb les quals s'estigui treballant. 

A llarg de les següents tasques i activitats, aniràs treballant indistintament amb els dos entorns de treball: important capes i taules en l'apartat **«Contingut»**, i visualitzant i simbolitzant aquestes mateixes capes i taules, en l'apartat **«Mapa»**. 

Per accedir al mapa, és necessari fer un clic sobre el botó, entrada o menú **MAPA**. Actualment, a AGOL conviuen dues aplicacions del visor de mapes: el **_Map Viewer Classic_** i el **_Map Viewer Nou_**. Per navegar o alternar entre un o altre visor de mapes, existeix en l'angle superior dret de la finestra general, un enllaç que mostrarà el text **_Obre-ho al Map Viewer Classic_** o **_Obre-ho al Map Viewer Nou_**, segons quin sigui el visor actiu en cada moment. 

#### ![t!](/static/learning.png) TASCA GUIADA #2a: Visualitzar el teu primer conjunt de dades. 

* Abans de començar a carregar i visualitzar cap conjunt de dades, mou-te al nou visor de mapes d'AGOL. A continuació, al panell d'eines situat a l'esquerra, fes un clic sobre l'apartat **Capes**.  

![carrega_capa.png!](/static/carrega_capa.png "Carregar un nova capa") 

* En fer un clic, es desplega un nou panell on s'hi pot veure el botó **+ Afegeix una capa**. Fes un clic sobre aquest botó, i comprova com de forma automàtica, es mostren les tres capes que acabes d'importar fa una estona des de l'apartat **«Contingut»**.  

![carrega_capa2.png!](/static/carrega_capa2.png "Carregar un nova capa") 

* En aquest panell emergent on es mostren les capes disponibles, hi ha dues possibles accions a fer: 

  * un clic sobre el nom de la capa, fet que mostrarà informació sobre la capa i la possibilitat de carregar-la al mapa o bé, 
  * un clic sobre el botó inferior que mostra una icona en forma de creu, permetent que la capa es carregui directament al visor. 

* Fes un clic sobre aquest darrer botó (ressaltat de color vermell en la següent imatge) visible en l'espai de la capa **airbnb_Girona**: 

![carrega_capa3.png!](/static/carrega_capa3.png "Carregar un nova capa") 

* Automàticament, en carregar-se la capa al visor de mapes, apareix un nou panell al costat dret del visor. Aquest nou panell (**Propietats**), compta amb diversos apartats: 

  * Informació. 
  * Simbologia. 
  * Aparença. 
  * Interval visible. 
  * Interval d'actualització. 
  * Ordre de visualització d'entitats. 
  * Paràmetres personalitzats. 

![carrega_capa4.png!](/static/carrega_capa4.png "Propietats de la capa") 

* Abans de procedir a modificar l'estil de la capa d'allotjaments d'airbnb, comprova les accions que es poden dur a terme al panell de capes d'AGOL, on es mostren en tot moment, les capes que estan carregades al mapa. Així doncs, al panell situat a l'esquerra del visor, fes un clic sobre el botó **"tornar enrere"** que es representa amb la icona **(<)** i, quan es mostri el panell principal de capes, on podràs veure l'entrada que acabes d'afegir (airbnb_Girona), fes un clic sobre el botó que mostra els tres punts (**...**) per tal d'accedir a un conjunt d'accions relacionades amb la capa. Aquestes accions permeten: 

  * ajustar la vista a l'extensió de la capa. 
  * modificar el nom. 
  * veure'n la taula d'atributs. 
  * consultar les propietats. 
  * eliminar la capa del panell de capes. 

![propietats_capa.gif!](/static/propietats_capa.gif "Propietats de la capa carregada al panell") 

A continuació, et mostrarem la manera de modificar l'aparença o estil de la capa de punts, per tal de millorar l'estètica i adaptar-la al teu gust o necessitat. 

#### _Modificar la simbologia bàsica dels punts: l'ús de simbologia única._ 

El panell de propietats de les capes (situat a la dreta) pot ocultar-se o tancar-se, i es pot reobrir de nou bé des del menú contextual de la capa, bé des del botó **Propietats** (visible en la barra lateral situada a la dreta del visor): 

![propietats_capa2.gif!](/static/propietats_capa2.gif "Propietats de la capa carregada al panell") 

* A continuació, obre o accedeix a les propietats de la capa, i desplega l'apartat relatiu a la simbologia de punt que per defecte, s'ha aplicat a la capa de punts que representa l'oferta d'allotjament d'airbnb. Fes un clic sobre l'opció **Edita l'estil de la capa**, i observa les opcions que es destaquen a continuació. 

* La simbologia per defecte aplicada a cadascun dels punts, és una simbologia única i, en el cas de voler-la modificar, cal que primer facis un clic sobre el botó **Opcions d'estil** i, a continuació, cal que facis un nou clic sobre l'apartat **Estil de símbol** per tal d'accedir a l'editor de simbologia. 

![simbologia.gif!](/static/simbologia1.gif "Editor de simbologia") 

* Per començar, selecciona una de les formes bàsiques (p.e. cercle), i tal i com es mostra a continuació, canvia el color i el gruix del contorn del cercle, el seu color d'emplenament, la mida del cercle, i el seu valor de transparència, entre d'altres aspectes. En acabar, recorda a desar els canvis tot fent un clic al botó **Fet**. Comprova com, a mesura que vas fent canvis, l'aparença del símbol sobre el mapa, es va actualitzant. 

![simbologia2.gif!](/static/simbologia2.gif "Editor de simbologia") 

* Per tal de donar per finalitzada aquesta segona tasca, guarda el mapa per tal que posteriormente el puguis reobrir, i recuperar el treball fet fins al moment. Per a desar un mapa (aquest es desarà en l'apartat «CONTINGUT», cal fer un clic sobre l'apartat **Desa-ho i Obre-ho** situat al panell esquerre del visor, seleccionar l'opció **Desa** i, a la finestra emergent, assignar un nom al mapa, i una ubicació (p.e. carpeta GIRONA).   

![desa_mapa.gif!](/static/desa_mapa.gif "Desar el mapa de treball") 

#### ![t!](/static/pencil.png) TASCA #2b: El teu torn. Edita la simbologia dels allotjaments airbnb de Barcelona. 

En aquesta tasca hauràs de carregar la capa d'allotjaments airbnb de Barcelona, i editar-ne la simbologia. Així, caldrà que modifiquis tots els paràmetres que siguin necessaris, fins que els punts mostrin l'aspecte que es pot veure a continuació. Aprofita també per canviar els noms de les capes (visibles al panell de capes), tal i com es mostren a la llegenda de la següent imatge: 

![simbol_bcn.png!](/static/simbol_bcn.png "Simbologia de punt") 

En acabar, desa el mapa a la carpeta **BARCELONA**.  

#### _Modificar la simbologia bàsica dels punts: l'ús de heatmaps o mapes de calor._ 

Més enllà de modificar el símbol que representa la ubicació o localització de cadascun dels punts d'una capa qualsevol, una altra possible representació de les entitats passa per dissenyar un mapa de calor o _heatmap_. Amb aquest tipus de representació, el que s'aconsegueix és mostrar la densitat o la concentració de punts, damunt del territori.  

#### ![t!](/static/learning.png) TASCA GUIADA #3a: Dissenyar un _heatmap_ a partir d'una capa de punts. 

* La primera passa que cal que duguis a terme és activar la capa d'allotjaments airbnb de Girona per tal que les modificacions estètiques, tinguin lloc sobre aquesta capa en concret. Així doncs, accedeix a l'apartat **Estils**, obre el desplegable de capes i selecciona la que fa referència a Girona. A continuació, fes un clic sobre l'opció **Mapa de calor**, i accedeix a les **opcions d'estil**: 

![alterna_capa.gif!](/static/alterna_capa.gif "Crea un mapa de calor") 

* Una vegada obert l'editor d'estil, podràs modificar dos paràmetres: 

  * La mida o el radi de cerca a l'entorn de cada punt per tal de determinar el valor de densitat i per tant, la forma i color del _heatmap_. 
  * La paleta de colors a aplicar, i la transició entre els colors que conformen la paleta seleccionada. 

![heatmap.gif!](/static/heatmap.gif "Crea un mapa de calor") 

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA #3b: El teu torn. Dissenya un heatmap per a la capa d'allotjaments airbnb a Barcelona. 

Ara, activa la capa d'allotjaments airbnb de Barcelona, i dissenya un nou _heatmap_ tot practicant amb els diversos paràmetres que es poden controlar o modificar. Recorda a desar el mapa, en acabar. 

#### _Modificar la simbologia bàsica dels punts: l'aplicació de filtres i efectes._ 

En ocasions, pot resultar necessari o imprescindible treballar només amb una part de la mostra, en lloc de fer-ho amb totes les observacions de la mostra o capa d'informació. En aquests casos, es pot fer servir l'eina de filtre per tal de mostrar únicament aquelles observacions que responen afirmativament a una o més condicions. El filtrat de capes, es duu a terme des del botó de filtre situat al panell dret d'AGOL: 

![filtre.png!](/static/filtre.png "Control de filtres") 

#### ![t!](/static/learning.png) TASCA GUIADA #4a: Filtrar entitats i aplicar efectes de visualització. 

* Activa la capa d'allotjaments de Girona, i aplica'ls una simbologia bàsica de punt, de color blau, amb una mida per exemple, de 20 punts. 

![filtre.gif!](/static/filtre.gif "Modificar la simbologia de punt") 

* A continuació, activa l'eina de filtre, i crea una nova expressió per a filtrar, i que per tant només es mostrin, aquells establiments o allotjaments el preu dels quals sigui inferior als 50€. El valor a partir del qual filtrar la mostra es pot introduir manualment, o bé es pot utilitzar l'histograma que mostra la distribució dels valors de la mostra, a la vegada que indica quin és el valor mig. 

![filtre2.png!](/static/filtre2.gif "Aplica un filtre") 

Una vegada creat un filtre, sempre se'n pot afegir un de nou, fent clic sobre el botó **+ Afegeix una expressió**. I també es pot duplicar o eliminar, fent un clic sobre el botó amb tres punts que es mostra a la capçalera del filtre i escollint l'opció **Suprimeix l'expressió**. 

![filtre2.png!](/static/filtre2.png "Aplica un filtre") 

![filtre3.png!](/static/filtre3.png "Aplica un filtre") 

La darrera qüestió que cal tenir present quan es defineixen dues o més condicions/expressions, és la relació que s'estableix entre les mateixes. Es pot escollir que només es mostrin aquelles entitats que compleixen totes i cadascuna de les expressions definides o bé, que en compleixin com a mínim una: 

![filtre4.png!](/static/filtre4.png "Aplica un filtre") 

* Una vegada només siguin visibles els allotjaments el preu del qual és inferior als 50€, procediràs a aplicar efectes de visualització a totes les entitats de la capa. Per a dur a terme aquesta acció, fes un clic sobre l'apartat **Efectes** i, en la finestra emergent, activa en primer lloc l'opció d'**Ombreig** i a continuació, l'opció **Flor** tot ajustant els paràmetres tal i com es mostra a continuació: 

![efecte1.gif!](/static/efecte1.gif "Aplica un efecte") 

Les diferents possibilitats d'efectes, també es poden aplicar únicament sobre alguns dels elements de la mostra, a través d'una expressió. En el següent exemple, es mostra com aplicar un efecte concret sobre aquelles entitats que compleixin el criteri definit a través d'una expressió. A la resta d'entitats, se'ls aplicarà un altre efecte diferent (p.e ombreig vs desenfocament). 

* Al panell principal d'efectes, fes un clic sobre el botó **Específic de l'entitat** i, a la finestra emergent, fes un clic sobre el botó **+ Afegeix una expressió**. L'expressió ha de servir per a distingir els allotjaments que es corresponen a una habitació privada. Una vegada definida l'expressió, selecciona l'efecte **Ombreig + desenfocament**. 

![efecte2.gif!](/static/efecte2.gif "Aplica un efecte") 

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA #4b: El teu torn. Filtra allotjaments a Barcelona i aplica'ls efectes de visualització. 

Activa la capa d'allotjaments airbnb de Barcelona i, en primer lloc, aplica una simbologia de punt bàsica. A continuació, aplica un filtre de manera que només es mostrin aquells allotjaments el preu del qual sigui superior als 250€ i que a més, el tipus d'allotjament sigui de casa o apartament sencer. Una vegada només es mostrin les entitats que compleixen ambdues expressions, aplica un efecte específic per a l'entitat, sobre aquells punts el preu dels quals, superi els 500€. Recorda a desar el mapa, en acabar.

## ![t!](/static/justified.png) 2. La visualització de dades amb simbologia segons el valor d'atribut. 

Fins al moment, s'ha mostrat com simbolitzar entitats d'una capa de punts, a partir de simbologia única i en base a la seva localització. En qualsevol cas, AGOL permet aplicar diferents colors, mides i símbols a cadascun dels elements o entitats d'una capa, en funció d'algun dels valors emmagatzemats en les diferents columnes de la seva taula d'atributs. Els valors que es poden utilitzar per a definir l'aparença de les entitats, poden ser qualitatius o quantitatius.    

### ![t!](/static/arrow-right.png) 2.1 Utilitzar símbols graduats: valors quantitatius 

Una primera opció de simbolització, és la d'utilitzar símbols graduats la mida dels quals, depèn del valor que cadascuna de les entitats tingui en una columna particular de la taula d'atributs. 

#### ![t!](/static/learning.png) TASCA GUIADA #5a: Aplicar simbologia de símbols graduats 

* Activa la capa d'allotjaments de Girona, i elimina els efectes i filtres que s'havien aplicat en exercicis anteriors. Pots deixar com a simbologia bàsica, un cercle de qualsevol color. 

![graduat1!](/static/graduat1.png "Simbologia graduada") 

* Fes un clic sobre el botó **Estils** i a continuació, sobre la opció **+ Camp**. D'entre tots els camps o columnes numèriques que es mostren, selecciona la columna **_PREU_** i a continuació, fes un clic sobre el botó **Afegeix**. Automàticament apareixeran noves opcions de simbolització: 

 * Recomptes i quantitats (mida) 
 * Recomptes i quantitats (color) 
 * Color i mida 
 * Tipus (simbols únics)). 

* Per defecte, i per tractar-se d'un camp o columna que emmagatzema valors de caràcter numèric, la simbologia que s'aplicarà es la de **Recomptes i quantitats (mida)**. 

![graduat2!](/static/graduat2.gif "Simbologia graduada") 

* La simbologia graduada que es genera per defecte, es pot editar tot fent un clic sobre el botó **Opcions d'estil**. Així, fes un clic sobre aquest botó per accedir a les diferents opcions d'edició. 

![graduat3!](/static/graduat3.png "Simbologia graduada") 

* En primer lloc, podràs decidir a partir de quin valor mínim o màxim s'aplicarà la diferent simbologia. Per defecte, AGOL agafa els valors mínims i màxims de la capa a l'hora de definir el símbol de mida mínima i el símbol de mida màxima. En qualsevol cas podràs modificar aquest comportament movent amunt i avall, els botons ressaltats en color groc, en la figura anterior. Ajusta els valors originals als nous valors **50** i **600**, respectivament. 

* A més, pots ajustar la mida del cercle mínim i del cercle màxim manualment, tot indicant la mida mínima i màxima en l'apartat **Interval de mides** (ressaltat en color morat en la figura anterior). 

* Finalment, centra la teva atenció en l'histograma que es mostra destacat de color blau, on podràs comprovar com es distribueixen els valors relatius al preu al llarg de la sèrie, a més d'oferir-te informació sobre el valor mig i la desviació estàndard.  

![graduat4.png!](/static/graduat4.png "Simbologia graduada") 

![t!](/static/info.png) A banda de la classificació automàtica que AGOL fa per tal de configurar les diferents mides del símbol escollit, aquesta també es pot fer de manera controlada per part de l'usuari. Activar el botó **Classifica les dades** t'oferirà tot un conjunt de noves possibilitats, podent gestionar el mètode de classificació, o el nombre de rangs, per exemple. 

* Fes un clic sobre el botó **Classifica les dades**. 

![graduat5.png!](/static/graduat5.png "Simbologia graduada") 

* En primer lloc, deixa com a mètode **Intervals iguals** i pel que fa al nombre d'intervals, escriu **5**. Ajusta la mida inicial i final del símbol en l'apartat **Interval de mides** fins aconseguir una representació adient. 

![graduat6.png!](/static/graduat6.png "Simbologia graduada segons intervals iguals") 

![t!](/static/info.png) Amb aquest mètode d'intervals iguals, cada rang de valors té la mateixa amplitud, i es pot donar el cas que no hi hagi entitats representades en algun dels rangs definits. En aquest tipus de classificació, si esculls un nombre de rangs imparell, gairebé sempre existirà visualment, un rang o categoria mitja mentre que en el cas d'escollir un nombre de rangs parell, les entitats o se situaran visualment, en la part alta o en la part baixa de la classificació. 

* Modifica ara, el mètode de classificació d'intervals iguals a **Quantils**. Pots mantenir el nombre d'intervals a **5**. 

![graduat7.png!](/static/graduat7.png "Simbologia graduada segons quantils") 

![t!](/static/info.png) Com pots comprovar en actualitzar-se la vista del mapa, l'aspecte de la capa i la informació que d'ella se'n desprèn han canviat amb relació a la visualització anterior. Amb el mètode de quantils, AGOL crearà els rangs de manera que a cada interval, hi hagi més o menys el mateix nombre d'entitats representades. Així doncs els intervals dels rangs seran diferents entre sí, ja que el que compta és el nombre d'elements assignats a cadascun d'ells. Amb aquest mètode, visualment, es normalitza i s'homogeneïtza la representació ja que no hi ha capa interval que predomini per damunt dels altres. Fixa't en les imatges a continuació i la definició dels intervals. La primera imatge es correspon a una classificació per intervals iguals mentre que la segona, es correspon a una classificació utilitzant el mètode de quantils: 

![graduat8.png!](/static/graduat8.png "Simbologia graduada per intervals iguals") 
![graduat9.png!](/static/graduat9.png "Simbologia graduada per quantils") 

* Modifica novament el mètode de classificació i, aquesta vegada, selecciona l'opció que duu per nom **Talls naturals**. Observa el resultat obtingut: 

![graduat10.png!](/static/graduat10.png "Simbologia graduada segons talls naturals") 

![t!](/static/info.png) A diferència dels mètodes anteriorment vistos, aquest no es basa ni en l'amplitud dels intervals ni en el nombre d'entitats que cauen dins de cadascun dels intervals, sinó que es basa en l'anàlisi de la sèrie de dades i del valor numèric que s'està representant. Cada vegada que es detecta una ruptura natural en la sèrie de dades, es defineix un límit d'interval, fins a obtenir el nombre d'intervals definits per l'usuari. 

* De nou, modifica el tipus de mètode classificació escollint aquesta vegada, la **Desviació estàndard**. I en acabar, fes un doble clic sobre el botó **Fet** per desar els canvis en la simbologia. 

![graduat11.png!](/static/graduat11.png "Simbologia graduada segons desviació estàndard") 

![t!](/static/info.png) Aquest mètode de classificació resulta especialment interessant ja que en utilitzar el valor de la desviació estàndard per definir els intervals de la classificació, a banda d'organitzar els elements en intervals, ofereix una informació addicional: la presencia de valors extrems, anòmals o _outliers_ en la sèrie. Aquelles entitats que es classifiquen en intervals extrems (per sobre i per sota), allunyades a més de dues desviacions estàndard de la mitja, indiquen la presència de valors anormalment alts o baixos.   

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA #5b: El teu torn. Representa els preus dels allotjaments de Barcelona 

Activa la capa d'allotjaments de la ciutat de Barcelona, i representa la variable relativa al preu (o qualsevol altra variable numèrica que consideris) utilitzant una classificació automàtica per defecte, i a continuació ves provant diferents mètodes de classificació, intervals, i tipologies i mides de símbol, fins a trobar el disseny que més et convenci. Recorda a desar el mapa en acabar.   

### ![t!](/static/arrow-right.png) 2.2 Utilitzar colors aleatoris i colors graduats: valors qualitatius i quantitatius 

A banda de la simbologia per simbols graduats, una altra manera de representar diferents magnituds és utilitzar paletes de colors graduats. D'aquesta manera, pot representar-se també una variable numèrica com és el cas dels preus dels allotjaments a la ciutat de Girona. 

#### ![t!](/static/learning.png) TASCA GUIADA #6a: Aplicar simbologia de colors graduats 

* Obre el panell d'estils, i activa la capa dels allotjaments de Girona. A continuació, selecciona l'opció **Recomptes i quantitats (color)**, i seguidament fes un clic a **Opcions d'estil**. Escull com a símbol un cercle de mida 15 i escull la paleta de color **Orange 4**, amb una transparència del **25%**. 

![colors_graduats.gif!](/static/colors_graduats.gif "Simbologia de colors graduats")

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA #6b: El teu torn. Representa els preus dels allotjaments de Girona i de Barcelona amb colors graduats 

De la mateixa manera que has vist abans, aplica simbologia de colors graduats als allotjaments de Girona i Barcelona, en base al preu per nit dels mateixos, utilitzant però els diferents mètodes de classificació amb els quals has practica anteriorment, en el cas dels símbols graduats. Recorda a desar el mapa en acabar. 

#### ![t!](/static/question.png) Quin tipus de representació, t'aporta informació d'una manera més clara i precisa? Símbols graduats? Colors graduats? 

De la mateixa manera que en el cas de valors numèrics que representen una gradació de valors, el més aconsellable és emprar símbols graduats o colors graduats, en el cas de valors quantitatius que no representin una superfície de valors continus o, en el cas de valors qualitatius, el més adient és utilitzar una paleta de colors aleatoris. Aquesta tècnica és aplicable, per exemple, en el cas de voler representar temàticament un atribut com la tipologia d'allotjament. 

#### ![t!](/static/learning.png) TASCA GUIADA #7a: Aplicar simbologia de colors aleatoris 

* Obre l'editor d'estils d'AGOL, i modifica l'atribut que s'està simbolitzant. En comptes de **PREU**, escull la columna **TIPUS**. A continuació, desplaça't pel selector d'estils i fes un clic a **Tipus (símbols únics)** i a continuació, sobre **Opcions d'estil**. 

![random_colors.gif!](/static/random_colors.gif "Simbologia de colors graduats") 

* A l'apartat d'opcions d'estil, pots modificar els colors que per defecte s'assignen a cadascun dels valors únics que s'emmagatzemen a la columna TIPUS. Fes un clic sobre la barra de colors, i escull la paleta de colors aleatoris que més t'agradin. En seleccionar la nova paleta, AGOL refà la simbolització anterior. En qualsevol cas, si es vol escollir un color concret per a cada categoria, es pot fer tot fent un clic sobre el color assignat per defecte, i escollir un nou color. 

![random_colors2.gif!](/static/random_colors2.gif "Simbologia de colors graduats") 

* Ja per acabar, modificaràs el nom de les tres categories presents a la llegenda (els valors originals que es mostren s'extreuen directament de la columna de la taula d'atributs) pel seu equivalent al català. Per a fer-ho, tant sols has de fer un clic sobre cadascun dels noms, i reescriure'l. 

![etiquetes.gif!](/static/etiquetes.gif "Modificar les etiquetes") 

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA #7b: El teu torn. Representa la tipologia dels allotjaments de Barcelona amb colors aleatoris

Un cop hagis editat la simbologia dels allotjaments de la ciutat, recorda a desar el mapa.

### ![t!](/static/arrow-right.png) 2.3 Utilitzar símbols combinats per a la representació de dos atributs a la vegada 

En ocasions, pot resultar d'interès o pot ser necessari, representar de manera conjunta dues variables o atributs. En alguns casos pot ser fruit de la combinació de símbols graduats i colors graduats mentre que, en d'altres casos, la combinació serà de símbols graduats i colors aleatoris, per exemple. En el següent cas, practicaràs amb aquestes possibilitats. 

#### ![t!](/static/learning.png) TASCA GUIADA #8a: Com aplicar simbologia combinada, als allotjaments turístics de Girona? 

* Fes un _reset_ de la simbologia que fins al moment has estat aplicant a la capa d'allotjaments airbnb de Girona. Aplica un símbol base com pot ser un cercle d'un color qualsevol. 

* En primer lloc, hauràs de decidir quin atribut representaràs mitjançant un símbol graduat i quin atribut, mitjançant colors (graduats o aleatoris). El més lògic, semblaria indicar que es pot representar el preu amb un símbol graduat i, en color, la tipologia d'allotjament. 

* En primer lloc doncs, fes un clic sobre el botó d'estils, escull la columna o camp **PREU** com a atribut a simbolitzar i, d'entre totes les opcions, escull el tipus **Recomptes i quantitats (mida)**. Edita les opcions d'estil al teu gust, i accepta l'edició fent un clic al botó **Fet**. 

![preu.png!](/static/preu.png "Configurar símbol graduat") 

* En segon lloc, afegiràs a continuació el nou atribut a incorporar: la tipologia d'allotjament. Fes un clic sobre el botó **+ Camp**, escull la columna **TIPUS**, i edita les opcions d'estil (colors) i les etiquetes de cadascuna de les tres categories. A més del color, i del text, també pots utilitzar formes diferents per a cada categoria d'allotjament present al mapa. Un cop definit l'estil, fes clic tantes vegades com sigui necessari al botó FET, per tal d'acceptar totes les modificacions.  

![graduat12.png!](/static/graduat12.png "Configurar símbol graduat i colors") 

* En el cas que la teva cartografia no destaqui suficientment sobre el mapa base per defecte, aquest es pot modificar des de la icona o apartat **Mapa Base**, situat al panell esquerre del visor. 

![mapa_base.gif!](/static/mapa_base.gif "Modificar el mapa base del visor") 

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA #8b: El teu torn. Representa la tipologia i els preus dels allotjaments de Barcelona amb simbologia combinada. 

Representa els allotjaments de la ciutat de Barcelona, de manera que amb símbols graduats es mostri el preu mentre que, en colors aleatoris, es representin les tipologies de cadascun d'ells. En acabar, recorda a desar el mapa.

## ![t!](/static/justified.png) 3. Configuració dels elements emergents i de les etiquetes de les capes

Tots els elements o capes que es carreguen i són visibles a AGOL, són per defecte, consultables. Això implica que en fer un clic al seu damunt, s'obrirà una finestra emergent o _pop-up_ que mostrarà els atributs de cada element, emmagatzemats en la seva taula d'atributs. El comportament per defecte, és mostrar tots els atributs, amb el nom orginal de cadascuna deles columnes que conformen la taula d'atributs. En alguns casos, ja pot anar bé que sigui així mentre que, en altres ocasions, serà preferible mostrar només alguna d'aquesta informació. A continuació practicaràs amb la configuració de la finestra emergent de les entitats

#### ![t!](/static/learning.png) TASCA GUIADA #9a: Configurar la finestra emergent dels elements de la capa

* Partint de la base del mapa amb el qual has estat treballant fins ara, fes un clic sobre algun dels elements de la capa, per tal de desplegar la finestra emergent de propietats i veure'n el seu aspecte:

![popup.gif](/static/popup.gif "Obrir element emergent")

* A continuació fes un clic sobre el botó **Configurar elements emergents** al panell dret del visor. En la finestra emergent, esborra el contingut que es mostra en l'apartat **Títol**, i escriu el text **Allotjaments AirBnB a Girona**. 

![popup1.gif](/static/popup1.gif "Configura el títol de l'element emergent")

* Fes un clic sobre l'apartat **Llista de camps**, i en primer lloc, escriu un títol (**Característiques de l'allotjament**). A continuació, elimina totes les columnes que apareixen inicialment a excepció de **NOM**, **NITS_MINIMES**, **NOM_ALLOTJAMENT**, **PREU** i **TIPUS**. En acabar, tanca el panel de Finestres emergents.

![popup2.gif](/static/popup2.gif "Configura els camps visibles")

* Ara, fes un clic sobre el botó o apartat **Configura els camps**, al panell dret del visor i, en el nou panell emergent, ves fent clics sobre cadascuna de les columnes que seran visibles, i modifica'n el nom tal i com es mostra a continuació.

![popup3.gif](/static/popup3.gif "Configura el nom dels camps visibles")

* Modifica totes les columnes fins que la finestra emergent, mostri el següent aspecte:

![popup4.gif](/static/popup4.gif "Aspecte final de la finestra emergent d'atributs")

En el cas de le etiquetes, la situació i la manera de procedir és similar que en el cas de les finestres emergents. Entre d'altres aspectes, cal habilitar-les, escollir quin camp o columna es vol utilitzar per a dotar de valor a l'etiqueta, etc.

* Fes un clic al botó **Etiquetes** i a continuació, un clic sobre l'apartat **Habilita les etiquetes**. En l'apartat **Camp d'etiqueta**, substitueix la columna **TIPOLOGIA D'ESTABLIMENT** per la columna **NOM DE L'ESTABLIMENT**. 

![etiqueta1.gif](/static/etiqueta1.gif "Configuració inicial de l'etiqueta")

* Fes un clic sobre l'apartat **Estil d'etiqueta** i modifica'n el tipus de font (p.e. Avenir Pro Bold), la mida (11) , la ubicació (centre-centre), així com el color del text (blanc) i del seu corresponent _halo_ (negre).

![etiqueta2.gif](/static/etiqueta2.gif "Configuració del text de l'etiqueta")

* Finalment, pots ajustar l'escala de visualització de les etiquetes de manera que aquestes només siguin visibles a una determinada escala per tal d'evitar que hi hagi una superposició de textos, excessiu.

![etiqueta3.gif](/static/etiqueta3.gif "Configuració de l'escala de visualització de l'etiqueta")

* Desa el mapa.

#### ![t!](/static/pencil.png) TASCA GUIADA #9b: El teu torn. Configura la finestra emergent i les etiquetes dels allotjaments de Barcelona

Aplica els coneixements que acabes d'aquirir en l'activitat anterior, per modificar la finestra emergent de la capa d'allotjament de Barcelona. Edita i ajusta també, les etiquetes. En acabar, desa el mapa.

## ![t!](/static/justified.png) 4. Operacions senzilles entre punts i polígons

### ![t!](/static/arrow-right.png) 4.1 Recompte de punts en polígons

AGOL,és una plataforma que no només ofereix eines i recursos per a la visualització i publicació de dades en un mapa web, sinó que a més ofereix algunes eines i processos relacionats amb determinats anàlisis de dades, i rutines d'enriqueiment de les dades. Al llarg de les segünts línies treballaràs amb algunes d'aquestes eines i rutines. Les primeres operacions amb les que treballaràs, tenen per objectiu la interacció i els traspàs d'informació entre dues capes: una capa de punts i una capa de polígons.

#### ![t!](/static/learning.png) TASCA GUIADA #10a: Traspassar informació entre capes d'informació. Punts i polígons, i viceversa.

La primera tasca que hauràs de dur a terme és la d'importar una capa de polígons al teu entorn de treball. En la primera tasca que realitzat en aquest document, has vist com importar una nova capa des de l'apartat «**CONTINGUT**». El que hauràs de fer a continuació, és importar una capa amb les delimitacions de barri de la ciutat de Girona.

* Activa l'apartat **Contingut**, i importa la capa [Barris_Girona.geojson](/static/Barris_Girona.zip).

![importa_barris.gif](/static/importa_barris.gif "Importació d'una capa de polígons")

* Accepta els quadres de diàleg emergents, fins a finalitzar correctament la importació de la capa que conté les delimitacions de barri de la ciutat de Girona.

![importa_barris2.gif](/static/importa_barris2.gif "Importació d'una capa de polígons")

![t!](/static/info.png) Les eines i totes les opcions d'anàlisi d'AGOL, de moment només són accessibles a la versió clássica del visor. En un futur, aquestes mateixes opcions i eines seran accessibles al nou visor, malgrat de moment no estan soportades. Així doncs, a partir d'ara, treballaràs amb la versió clàssica del _Map Viewer_.

* Obre la capa que acabes d'importar, al **Map Viewer Classic**.

![obre_classic.gif](/static/obre_classic.gif "Obrir una capa al Map Viewer Classic")

* A continuació, carrega la capa d'allotjaments d'airbnb de la ciutat, desde l'apartat **+ Afegeix > Cerca capes**.

![obre_classic2.gif](/static/obre_classic2.gif "Obrir una nova capa al Map Viewer Classic")

* Una vegada visibles ambdues capes, aprofita per modificar el nom de les mateixes fent un clic sobre la icona amb tres punts i seleccionant l'opció de canviar nom (ressaltat en color groc), escollir el mapa base que millor et sembli (ressaltat en color verd) i, si vols, canviar la simbologia de cadascuna de les capes (ressaltat en color vermell).

![update_capes.png](/static/update_capes.png "Modificar característiques de les capes")

* A continuació, accedeix a l'apartat d'anàlisi, tot fent clic sobre el botó **Anàlisi** (ressaltat en color groc) visible a la capçalera del panell, situat a la part esquerra del visor. Alternativament, si fas un clic sobre l'apartat **Contingut** (ressaltat en color blau) veuràs com en situar el cursor sobre qualsevol de les capes, apareixen un seguit de botons sota el nom de la mateixa, i on justament el darrer botó, fa referència a l'apartat d'anàlisi.

![analisi1.png](/static/analisi1.png "Accedir a les eines d'anàlisi")

* Automàticament apareixerà un nou contingut al panell. En aquest panell (**Realitza l'anàlisi**) s'hi pot veure tot el conjunt d'eines i operacions que es poden dur a terme amb AGOL, organitzades en categories. En primer lloc, fes un clic sobre l'entrada **Resumir dades** per mostrar-ne el seu contigut i, d'entre totes les opcions possibles, escull la primera d'elles, la que duu per nom **Consolida punts**.

![analisi2.png](/static/analisi2.png "Exploració de les eines d'anàlisi")

![t!](/static/info.png) Aquest tipus d'operació, permet dur a terme un recompte del nombre d'entitats contingudes en una altra entitat (que pot ser una capa de polígons ja existent o bé una malla ortogonal o hexagonal que es pot generar al vol). I a més de l'esmentat recompte, també es poden realitzar determinats càlculs estadístics. En l'exemple a continuació, obtindràs el valor relatiu al nombre d'allotjaments existents en cada barri de la ciutat, a més de saber quin és el preu mig d'aquets allotjaments, en cada zona.

* En el punt 1 de l'eina **Consolida punts**, escull la capa d'allotjaments AirBnB. En el punt 2, deixa seleccionada l'opció Polígon i deixa que la capa seleccionada sigui la que conté la delimitació de barris de la ciutat.

![analisi3.png](/static/analisi3.png "Configuració de l'eina Consolida punts")

* En el punt 3, pots escollir com a estadística a executar, coneixer el preu mig de l'oferta en cadascun dels barris. Així doncs, escull el camp **PREU** i, com a estadística, **Mitjana**. Pasa al punt 5, i assigna un nom a la capa de sortida (p.e. **Allotjaments per barris**) i indica que aquest mateix resultat, es desi a la carpeta **GIRONA**. Finalment, abans d'executar l'anàlisi, desactiva la casella de l'opció **Utilitza l'extensió del mapa actual**, per tal que l'anàlisi tingui lloc utilitzant l'extensió de les capes, i no la de la vista. Fes un clic sobre el botó **EXECUTA L'ANÀLISI**.

![analisi4.png](/static/analisi4.png "Configuració de l'eina Consolida punts")

* Observa el resultat que es carrega al panell de capes, en finalitzar l'operació de recompte de punts en polígon.

![analisi5.png](/static/analisi5.png "Configuració de l'eina Consolida punts")

* Modifica a continuació la simbologia de la capa resultant (una combinació del límit de cadascun dels polígons amb un cercle que indica el nombre d'allotjaments per barri), per un clàssic mapa de coropletes. Fes un clic sobre el botó **Canvia l'estil** que hi ha just a sota del nom de la capa (**Allotjaments per barri**), i en l'apartat 1 de l'editor d'estil, deixa seleccionat el camp o columna **Count of Points**. En l'apartat 2, escull l'opció Recomptes i quantitats (color) i a continuació, fes un clic sobre el botó **OPCIONS**.

![analisi6.gif](/static/analisi6.gif "Configuració del mapa de coropletes")

* Fes un clic sobre el text **Símbols** per tal de modificar la paleta de colors per defecte. Escull per exemple, la darrera paleta del catàleg. A continuació, activa la casella Classifica les dades, i deixa per defecte el mètode **Talls naturals** i **4** classes (tot i que pots provar amb d'altres mètodes i classes, també). Finalment, aplica una transparència del **50%** a la capa i fes un clic al botó **D'ACORD** i **FET**.

![analisi7.gif](/static/analisi7.gif "Configuració del mapa de coropletes")

* Observa el resultat obtingut:

![analisi8.png](/static/analisi8.png "Resultat de la configuració del mapa de coropletes")

* Ara afegiràs un nou atribut a la capa que acabes d'editar. Fes de nou un clic sobre el botó **Canvia l'estil** i a continuació, fes un clic sobre el text **Afegeix un atribut** tot seleccionant la columna **Mean price**. A continuació fes un clic sobre el botó **OPCIONS** de l'apartat **Color i Mida**.

![analisi8.gif](/static/analisi8.gif "Configuració de la simbologia doble en un mapa de coropletes")

* En accedir a les opcions, podràs definir indistintament la simbologia relativa al recompte d'allotjaments, així com la del preu mig dels allotjaments de cadascun dels barris de la ciutat. Tan sols cal que facis un clic al botó **OPCIONS** de cadascun dels atributs, i configuris mides i colors al gust. Recorda en acabar, fer un clic sobre els botons inferiors **D'ACORD** i **FET**, per desar els canvis d'estil.

![analisi9.png](/static/analisi9.png "Configuració de la simbologia doble en un mapa de coropletes")

* Desa el mapa, com un mapa nou.


#### ![t!](/static/pencil.png) #10b: El teu torn. Analitza la distribució d'allotjaments per barri a la ciutat de Barcelona

En aquesta tasca hauràs de replicar l'exercici anterior de la ciutat de Girona però, aquesta vegada, amb el conjunt de dades de la ciutat de Barcelona. Les dades en qüestió, ja estan al teu espai de treball fruit de la importació feta en el primer exercici del document.

### ![t!](/static/arrow-right.png) 4.2 Assignació d'informació alfanumèrica per localització espacial

En l'apartat de resum de dades, a més del tipus d'anàlisi amb el que acabes de treballar, també és possible fer una assignació massiva d'atributs, d'una capa a una altra, en funció de la relació espacial que es pugui establir entre elles. En el cas dels allotjaments, una dada que manca o pot mancar en la capa, és conèixer i registrar a quin barri pertany cada allotjament. A continuació, assignaràs aquests valors tenint en compte la localització espacial de cada punt o establiment.

#### ![t!](/static/learning.png) TASCA GUIADA #11a: Traspassar informació entre capes d'informació. Relacions espacials.

* Desactiva la visualització de la capa **Allotjaments per barris** i activa novament la capa original d'allotjaments AirBnB. A continuació, fes un clic sobre el botó **Anàlisi**, desplega l'apartat **Resumir dades**, i fes un clic sobre l'eina **Uneix les entitats**.

* A continuació, has de configurar els els diversos punts de la següent manera. En el punt 1, tria la capa AirBnB com a **capa de destinació**. En el punt 2, escull la capa que conté la delimitació de barris. En el punt 3, fes un clic sobre el botó o icona de **Relació espacial**, del tipus **Completament dins**. Deixa el punt 4 amb els valors que apareixen per defecte i, en el punt 5, assigna un nom a la capa de sortida: **Allotjaments AirBnB Barris**. Desactiva la casella **Utilitza l'extensió de la capa actual**, i fes un clic sobre el botó **EXECUTA L'ANÀLISI**.

* Un cop carregada la capa al panell de capes, fes un clic sobre el botó d'estil, escull la columna **barris** com a valor a representar, i deixa per defecte el mètode Tipus (símbols únics) i observa el resultat. Ara es mostren els allotjaments, simbolitzats pel color del barri en el qual es troben. Adapta la paleta de colors al teu gust (també el tipus de símbol) i fes un clic al botó **FET**.

![allot_barris.gif](/static/allot_barris.gif "Resultat de la configuració del mapa de coropletes")






---- 

  

* <a href="https://www.flaticon.com/free-icons/arrow" title="arrow icons">Arrow icons created by Creative Stall Premium - Flaticon</a><br /> 
* <a href="https://www.flaticon.com/free-icons/pencil" title="pencil icons">Pencil icons created by Freepik - Flaticon</a><br /> 
* <a href="https://www.flaticon.com/free-icons/learning" title="learning icons">Learning icons created by Freepik - Flaticon</a><br /> 
* <a href="https://www.flaticon.com/free-icons/practice" title="practice icons">Practice icons created by Parzival’ 1997 - Flaticon</a><br /> 
* <a href="https://www.flaticon.com/free-icons/paragraph" title="paragraph icons">Paragraph icons created by Pixelmeetup - Flaticon</a><br /> 
* <a href="https://www.flaticon.com/free-icons/question-mark" title="question mark icons">Question mark icons created by Freepik - Flaticon</a><br /> 

 
