# Introducció a ArcGIS Online

Per a poder utilitzar la plataforma de publicació de dades i mapes online, cal en primer lloc disposar d'un usuari. En el cas de personal UdG (PDI, PAS, alumnat) se'ls pot donar d'alta un nou usuari a partir del codi d'usuari UdG. Una vegada generat l'usuari i havent creat un password, accedim a la pàgina web de la plataforma i procedim a identificar-nos: [ArcGIS Online](https://www.arcgis.com/index.html)
